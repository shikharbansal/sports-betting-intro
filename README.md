[Sports Betting](http://www.booboone.com/sports-betting/) can be regarded as a hobby or as a profession.
Sport events predicting is called sports betting. 

The issue concerning legality of this gambling kind is still under discussion. 
In the US only the state of Nevada has made sports betting legal. 

In Europe bookmaking offices can be seen here, there and everywhere and they are totally legal when having sports betting licenses.
Companies dealing sports betting should be strictly regulated by special financial funds so that to guarantee bettors that their winnings will be paid in time and in full and their made bets are safe and secure. 

A wide range of sports betting options is offered. It encompasses phone, Internet, and international betting. 
[Online sports betting](http://www.booboone.com/online-sports-betting/) wins more and more favor daily. Because bookmaking sites welcome bettors 24 hours 7 days a week and take bets on various matches held in different countries.

Sports bettors are often football, baseball or hockey fans who take to bookmaking so that to increase their interest in the outcome of an event. In such a case more people go to matches and sports increase in popularity significantly because of growing TV audience.
Bitter opponents of sports betting say that in future it will be bettors who will fix match results. Because even now several cases have been reported when bettors whose wagers are impressive pay to some teams to play as they are said to (the Chicago case when bettors bribed players to lose is a classic example) or even have killed players (the late Andre Escobar is the victim of mafia bettors).

So football or baseball games can turn out to be a crappy business. Because players become pawns in dirty hands of dishonest bettors.
But the thing about sports betting that both proponents and opponents need to realize is that it will take place notwithstanding the fact of its legality or non legality. 
So why not receive revenues from sports betting that now black market gets?
